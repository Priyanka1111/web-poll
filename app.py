#importing the libraries 
import argparse
import csv
import sys
import re
from flask import *
import toml
from werkzeug.utils import secure_filename
import os
import smtplib, ssl
import json
from flask import request
import tomli
# importing the modules
import os
import shutil
from new import user_result_csv
from login import user_login_csv,user_register_csv,user_forgot_csv
import hashlib
import logging

#Defined a flask name 
app = Flask(__name__)
app = Flask(__name__, template_folder='AdminLogin')

sender_script_filename = 'sendall.sh'
ssmtp_line_template = 'ssmtp {Address} < {EmailFName}'
send_line_template = ssmtp_line_template

#Creating a log file
logging.basicConfig(filename='service.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical message')

#home page view function
@app.route("/", methods = ['GET','POST'])
def home():   
    return render_template("login.html")
@app.route("/forgot_password")
def forgot_password():
    return render_template("forgot_password.html")

@app.route("/registration",methods=['GET','POST'])
def registration():
    if request.method == "POST":
        name = request.form['name']
        phone = request.form['phone']
        email = request.form['email']
        passwordddd = request.form['password']
        print(name,phone,email,passwordddd)
        # Declaring Password
        password = passwordddd

        dataBase_password = password
        # Encoding the password
        #creating a password hash
        hashed = hashlib.md5(dataBase_password.encode())

        # Printing the Hash
        print(hashed.hexdigest())

        dictss = [email,hashed.hexdigest(),name,phone]
        print(dictss)
        user = user_register_csv(dictss)
        print(user)
        if user['status'] == 1:

            return render_template("login.html",message="registration is completed")
        elif user['status'] == 5:

            return render_template("login.html",message="email already registered")
    else:
        return render_template("registration.html")

@app.route("/forgotpassword",methods=['GET','POST'])
def forgotpassword():
    if request.method == "POST":
        email = request.form['email']
        passwordddd = request.form['password']
        print(email,passwordddd)
        import hashlib
        # Encoding the password
        hashed = hashlib.md5(passwordddd.encode()) #creating hash password

        # Printing the Hash
        print(hashed.hexdigest())

        dictss = [email,hashed.hexdigest()]
        user = user_forgot_csv(dictss)

        print(user)
        if user['status'] == 1:
            return render_template("forgot_password.html",message="invalid user name")
        elif user['status'] == 2:
            return render_template("forgot_password.html",message="invalid user password")
        else:
            return render_template("login.html",message="message")
    else:
        return render_template("forgot_password.html")
    # return render_template("forgot_password.html")


@app.route("/index", methods = ['GET','POST'])
def index():
    print("home page")
    if request.method == 'POST':
        name = request.form['name']
        passwordddd = request.form['password']
        import hashlib

        # Declaring Password
        password = passwordddd
        # adding 5gz as password

        # Adding salt at the last of the password
        dataBase_password = password
        # Encoding the password
        hashed = hashlib.md5(dataBase_password.encode())

        # Printing the Hash
        print(hashed.hexdigest())

        dictss = [name,hashed.hexdigest()]
        user = user_login_csv(dictss)
        print(user)
        if user['status'] == 1:
            return render_template("login.html",message="invalid user name")
        elif user['status'] == 2:
            return render_template("login.html",message="invalid user password")
        else:
            return render_template("poll.html",message="message")
    else:
        return render_template("login.html")
#Preparing Toml file
def prepareTomlFile(data_tomlll):
    # print(data_toml)
    data_toml = data_tomlll[0]

    data = {"title":"POLL System",
    "contact details": {
        "name": data_toml['name'],
        "email": data_toml['email'],
        "Questions": {
      
      "1": {
        "question": data_toml['question'],
        "answer": data_toml['answer']
  }
  }
}
    }
    toml_string = toml.dumps(data)  # Output to a string

    output_file_name = "output.toml"
    with open(output_file_name, "w") as toml_file:
        toml.dump(data, toml_file)

#user questions file
@app.route("/poll/<pk>/<pk1>")
def poll(pk,pk1, delimiter=',', quotechar='"'):
    uidddNo=pk
    print(uidddNo)
    questionsList = []
    if pk == "9c0f63b311":
        pk = "pattern1"
    elif pk == "9c0f63b322":
        pk = "pattern2"
    elif pk == "9c0f63b333":
        pk = "pattern3"
    elif pk == "9c0f63b344":
        pk = "pattern4"
    pk2 = pk1[:len(pk1)-1]
    print(pk1)
    file_name = str(pk)+".toml"
    file_path = "polls/"+str(pk2)+"/"+file_name#os.path.join(folder, file_name)
    print("file_path",file_path)
    #toml file data from toml folder
    with open("toml_folder/"+str(pk)+".toml", mode="rb") as fp:
        print(fp)
        config = tomli.load(fp)
        print(config, type(config))
    # that toml file data stored in poll with user uniq id folder
    with open("toml_folder/"+str(pk)+".toml", 'rb') as fp1, \
        open(file_path, 'wb') as fp2:
        results = fp1.read()
        fp2.write(results)

    title = config["title"]
    
    print(title)
    print(request.method)
    if request.method != "POST":
        #checking the questions in a pattern
        if len(config['questions'])>0:
            print(len(config['questions']))
            # Fetching the list of all the files
            # confif_file = open("toml_folder/"+str(pk1)+".toml", mode="rb")
            # files = os.listdir(config_file)

            # # Fetching all the files to directory
            # for file_name in files:
            #     shutil.copy(confif_file+file_name, target+file_name)
            #     print("Files are copied successfully")
            if pk == "pattern1":
                pk = "9c0f63b311"
            elif pk == "pattern2":
                pk = "9c0f63b322"
            elif pk == "pattern3":
                pk = "9c0f63b333"
            elif pk == "pattern4":
                pk = "9c0f63b344"
            elif pk == "pattern5":
                pk = "9c0f63b355"
            print("pkkkkkkkkkkkkkk",pk)
            return render_template("questions.html",pk22 = pk,pk23 = pk1,pk = config,pknumber=len(config))
    elif request.method == "POST":
        ans = []
        if len(questionsList)>0:
            for l in questionsList:
                value = l.question
                print(value)
                options = request.form[value]
                print(options)
                ans.append(options)
                return render_template("success.html")
    return render_template("poll.html")

# user questions and answers taking from particulart pattern in toml file
@app.route("/question-submit/<pk1>/<pk>", methods = ['GET','POST'])
def submitQuestions(pk1,pk,delimiter=',', quotechar='"'):
    if pk1 == "9c0f63b311":
        pk1 = "pattern1"
    elif pk1 == "9c0f63b322":
        pk1 = "pattern2"
    elif pk1 == "9c0f63b333":
        pk1 = "pattern3"
    elif pk1 == "9c0f63b344":
        pk1 = "pattern4"
    elif pk1 == "9c0f63b355":
        pk1 = "pattern5"
    ans = []
    dataprepare = []
    if request.method == "POST":
        print(pk1)
        with open("toml_folder/"+str(pk1)+".toml", mode="rb") as fp:
            questionsList = tomli.load(fp)
            # print(config, type(config))
            
            print("questionsList",questionsList,len(questionsList))
        if len(questionsList['questions'])>0:
            for l in range(1,len(questionsList['questions'])+1):
                print(l)
                value = questionsList["questions"][str(l)]['text'].split(' ')[0]
                print(value,type(value))
                optionss = request.form.getlist(value)
                g = ""
                for i in optionss:
                    g += i+" | "
                options = g.rstrip(" | ")
                if len(options) == 1:
                    print("kjklsd")
                    options = request.form[value]
                
                print(options)
                # get_query = ('update user_table3 set quetionsid=%s,user_answer=%s where uniqId = %s')
                # print(get_query,(l['id'],options,pk))
                # ans.append({'qution_name':questionsList["questions"][str(l)]['text'],'answer':options})
                ans.append(options)

        print(ans)
        rangeV = len(ans)
        pk2 = pk 
        pk = pk[:len(pk)-1]
        try:
            with open('polls/'+str(pk)+'/results.csv', 'r') as csvfile:     # writing to csv file answers stored with questions
                fieldnames = ['UUID']    
                dictData = {'UUID': pk2}
            with open('polls/'+str(pk)+'/results.csv', 'a') as csvfile:     # writing to csv file answers stored with questions
                fieldnames = ['UUID']    
                dictData = {'UUID': pk2}
                # writer.writerow({'UUID': pk})
                for i in range(rangeV):
                    questinDynamic = "question "+str(i+1)
                    fieldnames.append(questinDynamic)  
                    print({questinDynamic: ans[i]})
                    dictData[questinDynamic] = ans[i]
                print(fieldnames)
                print(dictData)
                writer = csv.writer(csvfile)#csv.DictWriter(csvfile, fieldnames=fieldnames)   
                # writer.writeheader()  
                ans.insert(0,pk2)
                writer.writerow(ans)    
        except Exception as e:
            with open('polls/'+str(pk)+'/results.csv', 'w') as csvfile:     # writing to csv file answers stored with questions
                fieldnames = ['UUID']    
                dictData = {'UUID': pk2}
                # writer.writerow({'UUID': pk})
                for i in range(rangeV):
                    questinDynamic = "question "+str(i+1)
                    fieldnames.append(questinDynamic)  
                    print({questinDynamic: ans[i]})
                    dictData[questinDynamic] = ans[i]
                print(fieldnames)
                print(dictData)
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)   
                writer.writeheader()  
                writer.writerow(dictData)  
        print(fieldnames)      
    return render_template("success.html")

# uploading polls files
@app.route("/upload",methods = ['GET','POST'])
def upload():
    res = "failed"
    pattern = request.form.get('pattern')
    print("pattern",pattern)
    if request.method == "POST":
        qfile = request.files['contactscsvile']# this is the contact csv file
        print(qfile)
        filename = secure_filename(qfile.filename)
        print(filename)
        qfile.save(os.path.join(filename))# Contact csv file stored in our directory
        if "toml_filename" in request.files: # recieving toml file 
            tfile = request.files['toml_filename']
            print(tfile)
            if tfile.filename == '':
                print("file not selected")
            else:
                print(tfile)
                filename = secure_filename(tfile.filename) # saving toml file
                pattern = filename.split('.')[0]
                folder_path = "./toml_folder"
                
                tfile.save(os.path.join(folder_path, filename))
                
                # tfile.save(os.path.join(filename))
        # if "pattern" in request.form.get: # recieving pattern options
        
        print(pattern)
        if pattern != "":
            pass
            print(pattern)
            print("pattern2222222",pattern)
        mailresponse = main(pattern)
        res = "sent success"
    return render_template("success.html",result=res)

#mail sending function
def sendMailUser(recieverMail,toData):
    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"
    sender_email = "priyankapriyu235@gmail.com" #from mail address
    receiver_email = recieverMail
    password = "yivk kyde jcga woog"
    message = toData

    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message) #email sending to and from mail 


def init_argparser():
    """Initialize the command line parser."""
    commandData = "echo pymailgen body.txt contacts.csv --ssmtp header.txt"
    return "parser"

#csv file reading 
def read_data_file(data_file=None, delimiter=',', quotechar='"'):
    """Loads the data used to fill the emails."""
    data = []
    with open("contacts.csv", 'r') as f:
        lines = csv.DictReader(f, delimiter=delimiter, quotechar=quotechar)
        for l in lines:
            data.append(l)
    return data

# email address validation with regex
def is_valid_address(email_address):
    """Check the validity of an email address."""
    if re.match(r"[^@]+@[^@]+\.[^@]+", email_address):
        return True
    else:
        return False


def process(email_text, data, send_line_template,pattern):
    import uuid
    """Generates an email file for each item in the input data."""
    n = len(str(len(data)))
    uuiddd = uuid.uuid4()
    uuidNo = str(uuiddd).split("-")
    email_filename_template = 'email_{{:0{}d}}.txt'.format(n)
    # gets an empty sender script file
    with open(sender_script_filename, 'w') as f:
        pass
    i = 0
    uuiddd = uuid.uuid4()
    uuidNo = str(uuiddd).split("-")
    print(uuidNo)
    #create unique id directory
    sub_dir = "polls/"
    path1 = os.path.join(sub_dir, str(uuidNo[0]))
    os.mkdir(path1)
    # createsub directory
    sub_dir = "polls/"+str(uuidNo[0])
    folder_name = "emails"
    path2 = os.path.join(sub_dir, folder_name)
    os.mkdir(path2)
    if pattern == "pattern1":
        pattern = "9c0f63b311"
    elif pattern == "pattern2":
        pattern = "9c0f63b322"
    elif pattern == "pattern3":
        pattern = "9c0f63b333"
    elif pattern == "pattern4":
        pattern = "9c0f63b344"
    elif pattern == "pattern5":
        pattern = "9c0f63b355"
    

    for j, item in enumerate(data):
        #.polls/'+str(uuidNo[0])+
        # email_filename_template = './email_folder/email_{{:0{}d}}.txt'.format(n)
        email_filename_template = 'polls/'+str(uuidNo[0])+'/emails/email_{uidddNo}.txt'.format(uidddNo=str(uuidNo[0])+str(j))
        print("jjjjjj",email_filename_template)
        print(enumerate(data))
        print("itemmm",item)
        dict = [str(uuidNo[0])+str(j),item['Email'], item['Name'], item['FamilyName']]
        user_csv = user_result_csv(dict)
        #user questions url generated here
        urllink = "http://localhost:5000/poll/"+pattern+"/"+str(uuidNo[0])+str(j)
        if 'Blacklist' in item:
            if item['Blacklist'] != '':
                continue
        item['url'] = urllink
        email = email_text.format(**item)
        print("email",email)
        email_filename = email_filename_template.format(i)
        print(email_filename)
        addr = item['Email']
        if not is_valid_address(addr):
            print('Invalid email address "{}" at line {}'.format(addr, j + 1))
            continue
        with open(email_filename, 'w') as f:
            f.write(email)
        fields = {'Address': addr, 'EmailFName': email_filename}
        sendMail = sendMailUser(addr,email)
        send_line = send_line_template.format(**fields)
        print("send_line",send_line)
        with open(sender_script_filename, 'a') as f:
            f.write(send_line + '\n')
            f.write('echo "[$(date +\"%Y-%m-%d %T.%3N\")] Email #{} to {}" | tee -a auto_mailer.log'.format(i, addr) + '\n')
        sendall = "echo bash sendall.sh" 
        i += 1


def check_data(data):
    if len(data) < 1:
        print('Data file must contain at least one row.')
        sys.exit(1)
    if 'Email' not in data[0]:
        print('Data file must contain the "Email" column (case-sensitive).')
        sys.exit(1)


def main(pattern):
    parser = init_argparser()
    # args = parser.parse_args()

    data = read_data_file()#read_data_file(args.datafile)
    check_data(data)

    with open("body.txt", 'r') as f:
        body = f.read()
    headerss = open("header.txt", 'r')
    if  headerss is not None:
        with open("header.txt", 'r') as f:
            header = f.read()
        email_text = header + '\n' + body
        send_line_template = 'ssmtp -t < {EmailFName}'
    else:
        email_text = body
    process(email_text, data, send_line_template,pattern)


if __name__ == "__main__":
    app.run(debug=True, port=5000)
